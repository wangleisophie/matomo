<?php

/* @CorePluginsAdmin/themes.twig */
class __TwigTemplate_23bc30b2121d74e65ec6c712d4018dae2fd48f00ac975864364c420b18a7b6e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin.twig", "@CorePluginsAdmin/themes.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["plugins"] = $this->loadTemplate("@CorePluginsAdmin/macros.twig", "@CorePluginsAdmin/themes.twig", 3);
        // line 5
        ob_start();
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("CorePluginsAdmin_ThemesManagement")), "html", null, true);
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "
<div piwik-content-intro>
    <h2 piwik-enriched-headline>
        ";
        // line 11
        echo \Piwik\piwik_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html_attr");
        echo "
    </h2>

    <p>
        ";
        // line 15
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("CorePluginsAdmin_ThemesDescription")), "html", null, true);
        echo "

        ";
        // line 17
        if (($context["isMarketplaceEnabled"] ?? $this->getContext($context, "isMarketplaceEnabled"))) {
            // line 18
            echo "            ";
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), array("CorePluginsAdmin_TeaserExtendPiwikByTheme", (("<a href=\"" . call_user_func_array($this->env->getFunction('linkTo')->getCallable(), array(array("action" => "browseThemes", "sort" => "")))) . "\">"), "</a>"));
            echo "
        ";
        }
        // line 20
        echo "
        ";
        // line 21
        if ((($context["otherUsersCount"] ?? $this->getContext($context, "otherUsersCount")) > 0)) {
            // line 22
            echo "            <br/> ";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("CorePluginsAdmin_InfoThemeIsUsedByOtherUsersAsWell", ($context["otherUsersCount"] ?? $this->getContext($context, "otherUsersCount")), ($context["themeEnabled"] ?? $this->getContext($context, "themeEnabled")))), "html", null, true);
            echo "
        ";
        }
        // line 24
        echo "        ";
        if ( !($context["isPluginsAdminEnabled"] ?? $this->getContext($context, "isPluginsAdminEnabled"))) {
            // line 25
            echo "            <br/>";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("CorePluginsAdmin_DoMoreContactPiwikAdmins")), "html", null, true);
            echo "
        ";
        }
        // line 27
        echo "
    </p>
</div>

<div piwik-content-block content-title=\"";
        // line 31
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("CorePluginsAdmin_InstalledThemes")), "html_attr");
        echo "\"
     piwik-plugin-management>

    <p>
    </p>

    ";
        // line 37
        echo $context["plugins"]->getpluginsFilter();
        echo "

    ";
        // line 39
        echo $context["plugins"]->gettablePlugins(($context["pluginsInfo"] ?? $this->getContext($context, "pluginsInfo")), ($context["pluginNamesHavingSettings"] ?? $this->getContext($context, "pluginNamesHavingSettings")), ($context["activateNonce"] ?? $this->getContext($context, "activateNonce")), ($context["deactivateNonce"] ?? $this->getContext($context, "deactivateNonce")), ($context["uninstallNonce"] ?? $this->getContext($context, "uninstallNonce")), true, ($context["marketplacePluginNames"] ?? $this->getContext($context, "marketplacePluginNames")), ($context["isPluginsAdminEnabled"] ?? $this->getContext($context, "isPluginsAdminEnabled")));
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "@CorePluginsAdmin/themes.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 39,  98 => 37,  89 => 31,  83 => 27,  77 => 25,  74 => 24,  68 => 22,  66 => 21,  63 => 20,  57 => 18,  55 => 17,  50 => 15,  43 => 11,  38 => 8,  35 => 7,  31 => 1,  27 => 5,  25 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'admin.twig' %}

{% import '@CorePluginsAdmin/macros.twig' as plugins %}

{% set title %}{{ 'CorePluginsAdmin_ThemesManagement'|translate }}{% endset %}

{% block content %}

<div piwik-content-intro>
    <h2 piwik-enriched-headline>
        {{ title|e('html_attr') }}
    </h2>

    <p>
        {{ 'CorePluginsAdmin_ThemesDescription'|translate }}

        {% if isMarketplaceEnabled %}
            {{ 'CorePluginsAdmin_TeaserExtendPiwikByTheme'|translate('<a href=\"' ~ linkTo({'action':'browseThemes', 'sort': ''}) ~ '\">', '</a>')|raw }}
        {% endif %}

        {% if otherUsersCount > 0 %}
            <br/> {{ 'CorePluginsAdmin_InfoThemeIsUsedByOtherUsersAsWell'|translate(otherUsersCount, themeEnabled) }}
        {% endif %}
        {% if not isPluginsAdminEnabled %}
            <br/>{{ 'CorePluginsAdmin_DoMoreContactPiwikAdmins'|translate }}
        {% endif %}

    </p>
</div>

<div piwik-content-block content-title=\"{{ 'CorePluginsAdmin_InstalledThemes'|translate|e('html_attr') }}\"
     piwik-plugin-management>

    <p>
    </p>

    {{ plugins.pluginsFilter() }}

    {{ plugins.tablePlugins(pluginsInfo, pluginNamesHavingSettings, activateNonce, deactivateNonce, uninstallNonce, true, marketplacePluginNames, isPluginsAdminEnabled ) }}
</div>
{% endblock %}
", "@CorePluginsAdmin/themes.twig", "/var/www/html/matomo/plugins/CorePluginsAdmin/templates/themes.twig");
    }
}
