<?php
/**
 * Piwik - free/libre analytics platform
 *
 * @link http://piwik.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 *
 */
namespace Piwik\Plugins\MyPlugin\Reports;

use Piwik\Plugin\Report;
use Piwik\Report\ReportWidgetFactory;
use Piwik\Widget\WidgetsList;

abstract class Base extends Report
{
    protected function init()
    {
        $this->categoryId = 'General_Visitors';
    }
    public function configureWidgets(WidgetsList $widgetsList, ReportWidgetFactory $factory)
	{
    $widget = $factory->createWidget()->setName('Last visitor by browser');
    $widgetsList->addWidgetConfig($widget);
	}

}
